/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	//first function here:

function printBiodata() {
	let fullName = prompt("What is your Full Name?");
	let age = prompt("How old are you?");
	let location = prompt("Where do you live?");

	console.log("Hello, " + fullName + "!");
	console.log("You are " + age + " years old.")
	console.log("You live in " + location + ".")
}

printBiodata();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function favoriteArtists() {
	console.log("1. When Chai Met Toast");
	console.log("2. Asian Kung-Fu Generation");
	console.log("3. GirlStreet Boys");
	console.log("4. Burna");
	console.log("5. BNK48");
}

favoriteArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function favoriteMovies() {
	console.log("Dawn Patrol");
	console.log("Rotten Tommatoes Rating: 11%");
	console.log("Down Dog");
	console.log("Rotten Tommatoes Rating: 0%");
	console.log("Elves");
	console.log("Rotten Tommatoes Rating: 8%");
	console.log("Golden Gate");
	console.log("Rotten Tommatoes Rating: 7%");
	console.log("L.A Slasher");
	console.log("Rotten Tommatoes Rating: 7%");	
}

favoriteMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


printUsers();